package com.safebear.app;

import com.safebear.app.pages.LoginPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {

    @Test
    public void testLogin() {
    //step1 Confirm we're on the Welcome Page
    assertTrue(welcomePage.checkCorrectPage());
    //step2 Click on the login link and the Login Page loads
    welcomePage.clickOnLogin();
    //step3 Confirm that we're now on the login page
    assertTrue(loginPage.checkCorrectPage());
    //step4 Login with valid credentials
    loginPage.login("testuser","testing");
    //step5 Check that we're now on the user page
    assertTrue(userPage.checkCorrectPage());
    }

}
